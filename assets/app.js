let interfaceSplitter = document.getElementById("interfaceSplitter")
interfaceSplitter.draggable = true
let darkInterface = document.getElementById("darkInterface")
let lightInterface = document.getElementById("lightInterface")
let overLayer = document.getElementById("overLayer")
let selectedDays = 15
let priceList = {
    1: { cost: 120, name: "день" },
    15: { cost: 180, name: "дней" },
    30: { cost: 250, name: "дней" },
    60: { cost: 500, name: "дней" },
    180: { cost: 1000, name: "дней" }
}

function selectPeriod(e) {
    let periods = document.getElementsByClassName("period__day")
    for (const period of periods) {
        period.classList.remove("period__day_active")
    }
    e.target.classList.add("period__day_active")
    let days = parseInt(e.target.dataset.period)
    selectedDays = days
    let totalInfo = document.getElementById("totalInfo")
    totalInfo.innerHTML =
        priceList[selectedDays].cost + " руб / на " + selectedDays + " " + priceList[selectedDays].name
}

function selectFaq(e) {
    let faqs = document.getElementsByClassName("faq-item")
    for (const faq of faqs) {
        faq.classList.remove("faq-item_active")
    }
    e.target.parentElement.classList.add("faq-item_active")
}

function overLayerClose() {
    overLayer.style.display = "none"
    document.querySelector("#overLayer .content__body").innerHTML = ''
}
function overLayerShow() {
    overLayer.style.display = "flex"
}

function openModal(title, body) {
    document.querySelector("#overLayer .content__title").innerHTML = title
    document.querySelector("#overLayer .content__body").innerHTML = body
    overLayerShow()
}

function showProductFunction(e) {
    let partial = ""
    let title = e.target.title
    for (const item of e.path) {
        if (item.dataset.partial) {
            partial = item.dataset.partial
            break
        }
    }

    fetch("assets/partials/" + partial)
        .then((response) => {
            return response.text()
        })
        .then((text) => {
            openModal(title, text)
        })
}

function moveInterfaceImages(left) {
    interfaceSplitter.style.left = left + "px"
    darkInterface.style.clip = "rect(auto, " + left + "px, auto, auto)"
    lightInterface.style.clip = "rect(auto, auto, auto, " + left + "px)"
}

function playVideo(videoID) {
    let body =
        '<iframe width="640" height="480" loading="lazy" src="https://www.youtube.com/embed/' +
        videoID + '?autoplay=1" title="YouTube video player" ' +
        'frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ' +
        "allowfullscreen></iframe>"
    openModal("", body)
}

let observer = new IntersectionObserver((e) => {
    if(e[0].isIntersecting) {
        e[0].target.classList.add('in-view')
    } else {
        e[0].target.classList.remove('in-view')
    }
}, {threshold: 0.2});

function init() {
    interfaceSplitter.addEventListener("dragstart", function (e) {
        e.target.classList.add(`selected`)
    })

    interfaceSplitter.addEventListener("drag", function (e) {
        let left = parseInt(window.getComputedStyle(interfaceSplitter).left) + e.offsetX
        moveInterfaceImages(left)
    })

    interfaceSplitter.addEventListener("dragend", function (e) {
        let left = parseInt(window.getComputedStyle(interfaceSplitter).left) + e.offsetX
        moveInterfaceImages(left)
        e.target.classList.remove(`selected`)
    })

    let periods = document.getElementsByClassName("period__day")
    for (const period of periods) {
        period.addEventListener("click", selectPeriod)
        let days = parseInt(period.dataset.period)
        if (days == selectedDays) {
            period.classList.add("period__day_active")
        }
    }

    let faqs = document.getElementsByClassName("faq-question")
    for (const faq of faqs) {
        faq.addEventListener("click", selectFaq)
    }

    document.querySelector("#overLayer .content__close").addEventListener("click", overLayerClose)
    document.querySelector("#overLayer").addEventListener("click", overLayerClose)
    for (const productFunction of document.getElementsByClassName("functions__item")) {
        productFunction.addEventListener("click", showProductFunction)
    }

    observer.observe(document.querySelector('.intersection'));
}

document.addEventListener("DOMContentLoaded", init)
